This repository contains the slides and other materials related to the course "Sustainable development of simulation software",
given at the Department of Hydromechanics and Modelling of Hydrosystems at the University of Stuttgart, Germany.

The slides are automatically deployed via GitLab pages, view them at
[sustainable-simulation-software.gitlab.io/course-material](https://sustainable-simulation-software.gitlab.io/course-material/)

# Contents

The course covers the following topics:

- Introduction to Python
- Aspects of clean code
- Software design principles
- Software design patterns
- Testing
- Introduction to Git and Git workflows

# Student project

This course is accompanied by a student project developed along the semester.
A description of the project and its goals can be found [here](project/README.md)

# Acknowledgements

The authors of this material would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) - project number 442146713.
