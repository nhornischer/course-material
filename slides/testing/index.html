<!--
This is a template file, in which the revealjs-path has to be set.
Use the convenience script at the top level of the repository for creating the
website
-->

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta name="author" content="Bernd Flemisch">
        <meta name="author" content="Dennis Gläser">
        <title>Testing</title>
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

        <link rel="stylesheet" href="[[PATH_TO_REVEALJS]]/dist/reset.css">
        <link rel="stylesheet" href="[[PATH_TO_REVEALJS]]/dist/reveal.css">
        <link rel="stylesheet" href="[[PATH_TO_REVEALJS]]/dist/theme/custom_white.css">
        <link rel="stylesheet" href="[[PATH_TO_REVEALJS]]/plugin/highlight/atom-one-light.css">
    </head>
    <body>
        <div class="reveal">
            <div class="slides">

                <section id="title-slide"
                         data-background-image="https://images.t3n.de/news/wp-content/uploads/2014/07/test.jpg?class=hero-small"
                         data-background-opacity="0.5">
                     <div style="text-align:left; width:85%; background-color:rgba(255,255,255,0.8); box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.85);">
                         <h2 style="margin:50px">Testing</h2>
                     </div>
                </section>

                <section data-transition="slide-in slide-out">
                    <h2>Testing</h2>
                    <p>Why do we need tests?</p>
                    <ul>
                        <li class="fragment">They ensure our program works as intended</li>
                        <li class="fragment">They allow us to detect bugs early</li>
                        <li class="fragment">They allow us to fearlessly change our code</li>
                    </ul>
                </section>

                <section data-transition="slide-in slide-out">
                    <h2>Testing</h2>
                    <p>How should I write my tests?</p>
                    <ul>
                        <li class="fragment">Ensure that they are well readable & expressive</li>
                        <li class="fragment">Each test should test one specific thing</li>
                        <li class="fragment">Aim for a high test coverage</li>
                        <li class="fragment">Focus on the most common & the corner cases</li>
                        <li class="fragment">Tests should execute fast</li>
                    </ul>
                </section>

                <section data-transition="slide-in slide-out">
                    <h2>Testing</h2>
                    <p>Types of tests</p>
                    <img src="https://www.exasol.com/app/uploads/2020/06/testing_pyramid.png"/>
                </section>

                <section data-transition="slide-in slide-out">
                    <h2>Testing</h2>
                    <p>Regression tests</p>
                    <img src="https://miro.medium.com/max/656/0*gHOjr-KEd8erJZyR"/>
                    <p class="fragment"><small>
                        In particular for simulation software, one often does not know what the
                        "true" result is. Regression testing allows you to detect if
                        changes you introduce change the output of your simulator. Whether the
                        new result is "better" or "worse" has to be investigated...
                    </small></p>
                </section>

                <section data-transition="slide-in slide-out">
                    <h2>Testing</h2>
                    <p>What does a unit test look like?</p>
                    <ul>
                        <li class="fragment">Bring the test program into the required precondition</li>
                        <li class="fragment">Use the software unit to be tested in one single way</li>
                        <li class="fragment">Verify that this usage changed the state as expected</li>
                    </ul>
                </section>

                <section data-transition="slide-in slide-out">
                    <h2>Testing</h2>
                    <p>A simple example</p>
                    <pre><code data-trim data-noescape class="hljs python">
                        def test_empty_list_append_changes_size_to_1():
                            my_list = []       # precondition: empty list
                            my_list.append(1)  # usage: append an item
                            assert len(my_list) == 1  # verify result state
                    </code></pre>

                    <p class="fragment"><small>
                        Note: you should only test units of your own code base and
                        not those of other libraries, and especially not the standard
                        library. This is just for illustration purposes.
                    </small></p>
                </section>

                <section data-transition="slide-in fade-out">
                    <h2>Testing</h2>
                    <div style="width:100%; background-color:rgba(255,255,255,0.8); box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.85);">
                        <p>Test only one thing per test!</p>
                    </div>

                    <div class="fragment">
                        <pre><code data-trim data-noescape class="hljs python">
                            from mygeometryproject import Point, Vector, Segment, Rectangle

                            def test_stuff():
                                segment = Segment(Point(0.0, 0.0), Point(1.0, 0.0))
                                assert abs(segment.length - 1.0) < 1e-6

                                rectangle = Rectangle(Point(0.0, 0.0), Vector(1.0, 1.0))
                                assert abs(rectangle.area - 1.0) < 1e-6
                        </code></pre>
                    </div>

                    <p class="fragment"><small>
                        The name of this test does not tell the developer what went wrong
                        in case this fails.
                    </small></p>
                </section>

                <section data-transition="fade-in fade-out">
                    <h2>Testing</h2>
                    <div style="width:100%; background-color:rgba(255,255,255,0.8); box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.85);">
                        <p>Test only one thing per test!</p>
                    </div>

                    <pre><code data-trim data-noescape class="hljs python">
                        from mygeometryproject import Point, Vector, Segment, Rectangle

                        def test_segment_and_rectangle():
                            segment = Segment(Point(0.0, 0.0), Point(1.0, 0.0))
                            assert abs(segment.length - 1.0) < 1e-6

                            rectangle = Rectangle(Point(0.0, 0.0), Vector(1.0, 1.0))
                            assert abs(rectangle.area - 1.0) < 1e-6
                    </code></pre>

                    <p class="fragment"><small>
                        Now the name is more informative, but still, the log has to be checked
                        to see if <code>Segment</code> or <code>Rectangle</code> caused a failure.
                    </small></p>
                </section>

                <section data-transition="fade-in fade-out">
                    <h2>Testing</h2>
                    <div style="width:100%; background-color:rgba(255,255,255,0.8); box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.85);">
                        <p>Test only one thing per test!</p>
                    </div>

                    <pre><code data-trim data-noescape class="hljs python">
                        from mygeometryproject import Point, Vector, Segment, Rectangle

                        def test_segment():
                            segment = Segment(Point(0.0, 0.0), Point(1.0, 0.0))
                            assert abs(segment.length - 1.0) < 1e-6

                        def test_rectangle():
                            rectangle = Rectangle(Point(0.0, 0.0), Vector(1.0, 1.0))
                            assert abs(rectangle.area - 1.0) < 1e-6
                    </code></pre>

                    <p class="fragment"><small>
                        Now the test names directly hint at the source of a problem!
                    </small></p>

                    <p class="fragment"><small>
                        However, usually you want to test several aspects of a class, so it may be better to choose more detailed names
                    </small></p>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <div style="width:100%; background-color:rgba(255,255,255,0.8); box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.85);">
                        <p>Test only one thing per test!</p>
                    </div>

                    <pre><code data-trim data-noescape class="hljs python">
                        from mygeometryproject import Point, Vector, Segment, Rectangle

                        def test_segment_length():
                            segment = Segment(Point(0.0, 0.0), Point(1.0, 0.0))
                            assert abs(segment.length - 1.0) < 1e-6

                        def test_rectangle_area():
                            rectangle = Rectangle(Point(0.0, 0.0), Vector(1.0, 1.0))
                            assert abs(rectangle.area - 1.0) < 1e-6
                    </code></pre>

                    <p class="fragment"><small>
                        Moreover, you should test your functions for different conditions (e.g. different segment lengths & orientations)...
                    </small></p>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <div style="width:100%; background-color:rgba(255,255,255,0.8); box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.85);">
                        <p>You may use long test names!</p>
                    </div>

                    <pre><code data-trim data-noescape class="hljs python">
                        from mygeometryproject import Point, Segment, intersect

                        def test_parallel_segments_at_tolerance_distance_do_not_intersect():
                            tolerance = 1e-8
                            segment1 = Segment(Point(0.0, 0.0), Point(1.0, 0.0))
                            segment2 = Segment(
                                Point(0.0, tolerance),
                                Point(1.0, tolerance)
                            )
                            assert not intersect(segment1, segment2, tolerance=tolerance)
                    </code></pre>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <div style="width:100%; background-color:rgba(255,255,255,0.8); box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.85);">
                        <p>Write tests for every unit you implement!</p>
                    </div>
                    <p class="fragment">Why?</p>
                    <p class="fragment">
                        Otherwise you cannot know if your unit behaves exactly as you intended!
                    </p>
                    <p class="fragment">
                            This forces you to decouple that unit as much as possible from the rest of the code in order to facilitate testing it!
                    </p>
                </section>

                <section data-transition="slide-in fade-out">
                    <h2>Testing</h2>
                    <p>Consider this <code>Button</code> class:</p>
                    <pre><code data-trim data-noescape class="hljs python">
                        # button.py
                        from typing import Protocol

                        class SwitchableDevice(Protocol):
                            def turn_on(self): ...
                            def turn_off(self): ...

                        class Button:
                            def __init__(self, device: SwitchableDevice) -> None:
                                self._device = device
                                self._device.turn_off()
                                self._off = True

                            def poll(self):
                                if self._off:
                                    self._device.turn_on()
                                else:
                                    self._device.turn_off()
                                self._off = not self._off
                    </code></pre>
                    <p class="fragment"><small>
                        <code>Button</code> can control any switchable device, but how can we test
                        this class independent of any concrete switchable device implementation?
                    </small></p>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <p>Use a test double!</p>
                    <div class="fragment">
                        <pre><code data-trim data-noescape class="hljs python">
                            # test_button.py
                            from button import Button

                            class SpyDevice:
                                def __init__(self):
                                    self._is_on = False

                                def turn_on(self):
                                    self._is_on = True

                                def turn_off(self):
                                    self._is_on = False

                                @property
                                def is_on(self) -> bool:
                                    return self._is_on

                                @property
                                def is_off(self) -> bool:
                                    return not self._is_on

                            def test_button():
                                device = SpyDevice()
                                button = Button(device)

                                button.poll()
                                assert device.is_on

                                button.poll()
                                assert device.is_off
                        </code></pre>
                    </div>
                    <p class="fragment"><small>
                        There are different categories of test doubles ("mock" objects),
                        see for instance <a href="https://www.techtarget.com/searchsoftwarequality/tip/Inside-5-types-of-test-doubles">here</a>.
                    </small></p>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <p>An example for testing in Python with <em>pytest</em></p>
                    <pre><code data-trim data-noescape class="hljs python">
                        # transformation.py
                        from dataclasses import dataclass

                        @dataclass
                        class Point:
                            x: float
                            y: float

                        class Transformation:
                            def __init__(self,
                                         dx: float,
                                         dy: float) -> None:
                                self._dx = dx
                                self._dy = dy

                            def __call__(self, point: Point) -> Point:
                                return Point(
                                    x=point.x+self._dx,
                                    y=point.y+self._dy
                                )
                    </code></pre>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <p>Let's write a first test...</p>
                    <pre><code data-trim data-noescape class="hljs python">
                        # test_transformation.py
                        from transformation import Point, Transformation

                        def test_null_transformation():
                            null_transformation = Transformation(dx=0.0, dy=0.0)
                            origin = Point(x=0.0, y=0.0)
                            transformed = null_transformation(origin)
                            assert transformed.x == 0.0
                            assert transformed.y == 0.0
                    </code></pre>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <p>... and use pytest to execute it.</p>
                    <pre><code data-trim data-noescape class="hljs shell">
                        ~/testproject$ python -m pytest
                        ========================================= test session starts ==========================================
                        platform linux -- Python 3.8.10, pytest-6.2.5, py-1.11.0, pluggy-1.0.0
                        rootdir: /home/dennis/testproject
                        plugins: cov-3.0.0
                        collected 1 item

                        test_transformation.py .                                                                         [100%]

                        ========================================== 1 passed in 0.01s ===========================================
                    </code></pre>
                    <p class="fragment"><small>
                        pytest automatically scans the files for functions beginning with <code>test</code>,
                        but you can configure it as you wish (see <a href="https://docs.pytest.org/en/6.2.x/customize.html">here</a>).
                    </small></p>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <p>Let's test transformations along the axes...</p>
                    <pre><code data-trim data-noescape data-line-numbers="|11-25|26-44" class="hljs python">
                        # test_transformation.py
                        from transformation import Point, Transformation
                        from math import isclose

                        def test_null_transformation():
                            null_transformation = Transformation(dx=0.0, dy=0.0)
                            origin = Point(x=0.0, y=0.0)
                            transformed = null_transformation(origin)
                            assert transformed.x == 0.0
                            assert transformed.y == 0.0

                        def test_positive_transformation_along_x_axis():
                            point = Point(x=0.5, y=1.5)
                            transformation = Transformation(dx=1.5, dy=0.0)
                            transformed = transformation(point)
                            assert isclose(transformed.x, 2.0)
                            assert transformed.y == 1.5

                        def test_negative_transformation_along_x_axis():
                            point = Point(x=0.5, y=1.5)
                            transformation = Transformation(dx=-1.5, dy=0.0)
                            transformed = transformation(point)
                            assert isclose(transformed.x, -1.0)
                            assert transformed.y == 1.5

                        def test_positive_transformation_along_y_axis():
                            point = Point(x=1.5, y=0.5)
                            transformation = Transformation(dx=0., dy=1.5)
                            transformed = transformation(point)
                            assert transformed.x == 1.5
                            assert isclose(transformed.y, 2.0)

                        def test_negative_transformation_along_y_axis():
                            point = Point(x=1.5, y=0.5)
                            transformation = Transformation(dx=0., dy=-1.5)
                            transformed = transformation(point)
                            assert transformed.x == 1.5
                            assert isclose(transformed.y, -1.0)
                    </code></pre>
                    <p class="fragment"><small>
                        Now we are testing the corner cases, we should still
                        add one or more tests for other transformation vectors...
                    </small></p>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <p>pytest now finds five tests</p>
                    <pre><code data-trim data-noescape class="hljs shell">
                        ~/testproject$ python -m pytest -v
                        ========================================= test session starts ==========================================
                        platform linux -- Python 3.8.10, pytest-6.2.5, py-1.11.0, pluggy-1.0.0 -- /usr/bin/python3
                        cachedir: .pytest_cache
                        rootdir: /home/dennis/testproject
                        plugins: cov-3.0.0
                        collected 5 items

                        test_transformation.py::test_null_transformation PASSED                                          [ 20%]
                        test_transformation.py::test_positive_transformation_along_x_axis PASSED                         [ 40%]
                        test_transformation.py::test_negative_transformation_along_x_axis PASSED                         [ 60%]
                        test_transformation.py::test_positive_transformation_along_y_axis PASSED                         [ 80%]
                        test_transformation.py::test_negative_transformation_along_y_axis PASSED                         [100%]

                        ========================================== 5 passed in 0.02s ===========================================
                    </code></pre>
                </section>

                <section data-transition="fade-in slide-out">
                    <h2>Testing</h2>
                    <div style="width:100%; background-color:rgba(255,255,255,0.8); box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.85);">
                        <p>
                            Start writing tests as soon as you begin to implement a new feature (or write the test first,
                            see <a href="https://en.wikipedia.org/wiki/Test-driven_development">TDD</a>)! This drives you
                            to write loosely-coupled components with a more user-friendly API.
                        </p>
                    </div>
                </section>
            </div>
        </div>

        <script src="[[PATH_TO_REVEALJS]]/dist/reveal.js"></script>
        <script src="[[PATH_TO_REVEALJS]]/plugin/notes/notes.js"></script>
        <script src="[[PATH_TO_REVEALJS]]/plugin/markdown/markdown.js"></script>
        <script src="[[PATH_TO_REVEALJS]]/plugin/highlight/highlight.js"></script>
        <script>
            // More info about initialization & config:
            // - https://revealjs.com/initialization/
            // - https://revealjs.com/config/
            Reveal.initialize({
                hash: true,

                // Learn about plugins: https://revealjs.com/plugins/
                plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ]
            });
        </script>
    </body>
</html>
